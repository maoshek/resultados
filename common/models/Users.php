<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "users".
 *
 * @property int $id_user
 * @property string $dni
 * @property int $status_user
 * @property string $updated_at
 * @property int $role
 * @property string $password
 * @property string|null $email
 * @property string $fullname
 * @property string $accessToken
 * @property string $auth_key
 * @property string|null $password_reset_token
 * @property string $created_at
 * @property string $verification_code
 * @property string|null $username
 */
class Users extends ActiveRecord implements IdentityInterface
{

    const STATUS_DELETED = 0;
    const STATUS_INACTIVE = 5;
    const STATUS_ACTIVE = 10;

    const ROLE_USER = 10;
    const ROLE_ADMIN = 30;
    const ROLE_SUPERUSER = 40;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'users';
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                    'class' => 'yii\behaviors\TimestampBehavior',
                    'attributes' => [
                        ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                        ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                    ],
                    'value' => new Expression('NOW()'),
                ],
            ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['status_user', 'default', 'value' => self::STATUS_ACTIVE],
            ['status_user', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_INACTIVE, self::STATUS_ACTIVE, self::STATUS_DELETED]],

            [['username', 'updated_at', 'password', 'fullname', 'accessToken', 'auth_key', 'created_at'], 'required'],
            [['status_user', 'role'], 'integer'],
            [['updated_at', 'created_at'], 'safe'],
            [['password', 'email', 'password_reset_token'], 'string', 'max' => 255],
            [['fullname'], 'string', 'max' => 100],
            [['accessToken'], 'string', 'max' => 250],
            [['verification_code'], 'string', 'max' => 20],
            [['username'], 'unique'],
            ['username', 'username_existe','on'=>['create']],
            [['password_reset_token'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_user' => Yii::t('app', 'Id User'),
            'username' => Yii::t('app', 'Username'),
            'status_user' => Yii::t('app', 'Status User'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'role' => Yii::t('app', 'Role'),
            'password' => Yii::t('app', 'Password'),
            'email' => Yii::t('app', 'Email'),
            'fullname' => Yii::t('app', 'Fullname'),
            'accessToken' => Yii::t('app', 'Access Token'),
            'auth_key' => Yii::t('app', 'Auth Key'),
            'password_reset_token' => Yii::t('app', 'Password Reset Token'),
            'created_at' => Yii::t('app', 'Created At'),
            'verification_code' => Yii::t('app', 'Verification Code'),
            'username' => Yii::t('app', 'Username'),
        ];
    }

    public static function findIdentity($id)
    {
        return static::findOne(['id_user' => $id, 'status_user' => self::STATUS_ACTIVE]);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    public static function findByUsername($username)
    {
        $user = static::findOne(['username' => $username]);
        if($user->status_user == $user::STATUS_ACTIVE){
            // echo '<script> console.log("Si entró findByUsername '.$user->status_user.'") </script>';
            // echo die;
            return $user;
        }
    }

    public static function findById($id_user)
    {
        $user = static::findOne(['id_user' => $id_user]);
        if($user && ($user->status_user == $user::STATUS_ACTIVE)){
            // echo die;
            return $user;
        }
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status_user' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        /* Valida el password */
         /*
         * si el password es menor a 13, tiene el hash antiguo no seguro, 
         * se valida que este correcto y se cambia el hash
         */
          if (strlen($this->password) <= 13) {

            // echo '<br>pass tipo cript';
            if (password_verify($password, $this->password)) {
                // echo '<br>pass anterior correcto';
                $this->password = password_hash($password, PASSWORD_BCRYPT);
                $this->save();
                // return 1;
            } else
                return 0;
        } 

        
        if (password_verify($password, $this->password)) {

            return 1;
        } else
            return 0;
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }
    
    public static function roleInArray($arr_role)
    {
        return in_array(Yii::$app->user->identity->role, $arr_role);
    }
    
    public static function isActive()
    {
        return Yii::$app->user->identity->status_user == self::STATUS_ACTIVE;
    }

    public function getDocuments()
    {
        return $this->hasMany(Documents::className(), ['id_user' => 'id_user']);
    }

    public function randKey($str='', $long=0){

        $key = null;
        $str = str_split($str);
        $start = 0;
        $limit = count($str)-1;
        for($x=0; $x<$long; $x++)
        {
            $key .= $str[rand($start, $limit)];
        }

        return $key;
    }

    public function username_existe($attribute, $params) {
        
        //Buscar el username en la tabla
        $table = Users::find()->where("username=:username", [":username" => $this->username]);

        //Si el username existe mostrar el error
        if ($table->count() == 1) {
            $this->addError($attribute, Yii::t('app', 'This user already exists.'));
        }
    }
}
