<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "documents".
 *
 * @property int $id_document
 * @property int $user_id
 * @property string $document_name
 * @property string $document
 * @property string $publish_date
 *
 * @property Users $user
 */
class Documents extends \yii\db\ActiveRecord
{
    public $file_doc;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'documents';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'document_name', 'document', 'publish_date'], 'required'],
            [['user_id'], 'integer'],
            [['publish_date'], 'safe'],
            [['document_name'], 'string', 'max' => 200],
            [['document'], 'string', 'max' => 100],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['user_id' => 'id_user']],
            [['file_doc'], 'file'],
             [['file_doc'], 'file', 'maxSize' => 1024*1024*5, 'tooBig' => Yii::t('app','The maximum allowed size is').' 5 MB'],    //5 MB
             [['file_doc'], 'file',   'extensions' => 'jpg, jpeg, png, gif, pdf',
                'wrongExtension' => Yii::t('app','File {file} does not contain an allowed extension {extensions}')],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_document' => Yii::t('app', 'Id Document'),
            'user_id' => Yii::t('app', 'User ID'),
            'document_name' => Yii::t('app', 'Document Name'),
            'document' => Yii::t('app', 'Document'),
            'publish_date' => Yii::t('app', 'Publish Date'),
        ];
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id_user' => 'user_id']);
    }
}
