function getTimeRemaining(endtime) {
  var t = Date.parse(endtime) - Date.parse(new Date());
  var segundos = Math.floor((t / 1000) % 60);
  var minutos = Math.floor((t / 1000 / 60) % 60);
  var horas = Math.floor((t / (1000 * 60 * 60)) % 24);
  var dias = Math.floor(t / (1000 * 60 * 60 * 24));
 
  return {
    'total': t,
    'dias': dias,
    'horas': horas,
    'minutos': minutos,
    'segundos': segundos
  };
}
 
function initializeReloj(id, endtime) {
  var reloj = document.getElementById(id);
  var diaSpan = reloj.querySelector('.dias');
  var horaSpan = reloj.querySelector('.horas');
  var minutoSpan = reloj.querySelector('.minutos');
  var segundoSpan = reloj.querySelector('.segundos');
 
  function updateReloj() {
    var t = getTimeRemaining(endtime);
    diaSpan.innerHTML = t.dias;
    horaSpan.innerHTML = ('0' + t.horas).slice(-2);
    minutoSpan.innerHTML = ('0' + t.minutos).slice(-2);
    segundoSpan.innerHTML = ('0' + t.segundos).slice(-2);
    if (t.total <= 0) {
      clearInterval(timeinterval);
    }
  }
  updateReloj();
  var timeinterval = setInterval(updateReloj, 1000);
}