<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Users;

/* @var $this yii\web\View */
/* @var $model common\models\Users */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="users-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'username')->textInput(['maxlength' => true])->label(Yii::t('app', 'User').'*') ?>        
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'password')->passwordInput(['maxlength' => true])->label(Yii::t('app', 'Password').'*') ?>  
        </div>
        <div class="col-md-4">
            <?php 
            if(Yii::$app->user->identity->role == Users::ROLE_ADMIN):
                $roles = array('10' => 'Paciente');
            elseif(Yii::$app->user->identity->role == Users::ROLE_SUPERUSER):
                $roles = array('10' => 'Paciente', '30' => 'Administrador', '40' => 'Super Administrador');
            endif;

           echo $form->field($model, 'role')->dropDownList($roles)->label(Yii::t('app', 'User Type').'*') ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'fullname')->textInput(['maxlength' => true])->label(Yii::t('app', 'Full Name').'*') ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success btn-rounded']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
