<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Breadcrumbs;

/* @var $this yii\web\View */
/* @var $searchModel common\models\UsersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Users');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="users-index">

    <div class="row bg-title">
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title"><?= Html::encode($this->title) ?></h4> 
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12"></div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <?=  Breadcrumbs::widget([
                  'homeLink' => [ 'label' => Yii::t('app', 'Dashboard'), 'url' => Yii::$app->homeUrl,],
                  'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
               ]) ?>
        </div>
    </div>

    <div class="white-box">
    <p>
        <?= Html::a(Yii::t('app', 'Create User'), ['create'], ['class' => 'btn btn-success btn-rounded']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            // ['class' => 'yii\grid\SerialColumn'],

            // 'id_user',
            [
                'attribute' => 'username',
                'label' => Yii::t('app', 'Username'), 
                'contentOptions' =>['style'=>'text-align:center;', 'width' => '10%'],
                'headerOptions' =>['style'=>'text-align:center;', 'width' => '10%'], 
            ],
            [
                'attribute' => 'fullname',  
                'label' => Yii::t('app', 'Full Name'), 
                'contentOptions' =>['style'=>'text-align:left;', 'width' => '30%'],
                'headerOptions' =>['style'=>'text-align:left;', 'width' => '30%'], 
            ],
            
            // 'status_user',
            // 'updated_at',
            [
                'attribute' => 'role',
                'label' => Yii::t('app', 'User Type'), 
                'contentOptions' =>['style'=>'text-align:left;', 'width' => '20%'],
                'headerOptions' =>['style'=>'text-align:left;', 'width' => '20%'], 
                'filter'=>array("10"=>"Paciente","30"=>"Administrador","40"=>"Super Administrador"),
                'value' => function($data){
                    if($data['role'] == 10):
                        return "Paciente";
                    elseif ($data['role'] == 30):
                        return "Administrador";
                    elseif ($data['role'] == 40):
                        return "Super Administrador";
                    endif;
                },

            ],

            [
                'attribute' => 'email',
                'label' => Yii::t('app', 'Email'), 
                'contentOptions' =>['style'=>'text-align:left;', 'width' => '20%'],
                'headerOptions' =>['style'=>'text-align:left;', 'width' => '20%'], 
            ],
            [
                'class' => 'kartik\grid\ActionColumn', 
                'template' => '{delete}',
                'buttons' => [
                    /*'edit' => function($url, $model, $key) {
                        return Html::a('<i class="fa fa-pencil-square-o"></i>', ['users/update', 'id' => $model->id_user], [  'class' => 'btn-lg', 'title' => Yii::t('app', 'Edit'), 'style' => 'color:#81c700;']);
                    },*/

                    'delete' => function($url, $model, $key) {
                        return Html::a('<i class="fa  fa-trash-o"></i>', ['users/delete', 'id' => $model->id_user], ['class' => 'btn-lg', 'title' => Yii::t('app', 'Delete'), 'id' => 'btn-del', 'style' => 'color:red;',
                            'data' => [
                                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                    'method' => 'post',
                                ],
                            ]);
                    },
                ],
                'contentOptions' =>['style'=>'text-align:center;', 'width' => '30%'],
                'headerOptions' =>['style'=>'text-align:center;', 'width' => '30%'],
            ],
        ],
    ]); ?>

</div>


</div>
