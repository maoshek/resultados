<?php

use yii\helpers\Html;
use yii\widgets\Breadcrumbs;

/* @var $this yii\web\View */
/* @var $model common\models\Users */

$this->title = Yii::t('app', 'Create User');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="users-create">

    <div class="row bg-title">
        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
            <h4 class="page-title"><?= Html::encode($this->title) ?></h4> 
        </div>
        <div class="col-lg-4 col-md-2 col-sm-2 col-xs-12"></div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <?=  Breadcrumbs::widget([
                  'homeLink' => [ 'label' => Yii::t('app', 'Dashboard'), 'url' => Yii::$app->homeUrl,],
                  'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
               ]) ?>
        </div>
    </div>

    <div class="white-box">
	    <?= $this->render('_form', [
	        'model' => $model,
	    ]) ?>
	</div>

</div>
