<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\Users;
use \yii\web\Request;

/* @var $this yii\web\View */
/* @var $model common\models\Documents */
/* @var $form yii\widgets\ActiveForm */

$baseUrl =  (new Request)->getBaseUrl();
?>

<div class="documents-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
    	<div class="col-md-6">
    		<?= $form->field($model, 'user_id')->dropDownList(
                ArrayHelper::map(Users::find()->all(), 'id_user', 'username', 'fullname'), ['prompt' => Yii::t('app', 'Select Pacient')])->label(Yii::t('app', 'Pacient'))?>
    	</div>
    	<div class="col-md-6">
    		<?= $form->field($model, 'document_name')->textInput(['maxlength' => true]) ?>		
    	</div>
    </div>

    <?= $form->field($model, 'file_doc', [
               'inputOptions' => ['class' => 'dropify', 'data-height'=> "300", 'data-default-file' => $baseUrl.'/id/'.$model->document, 'data-show-remove'=> 'false']])->fileInput()->label(Yii::t('app', 'Document')) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
