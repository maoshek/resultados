<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use \yii\web\Request;
use yii\widgets\Breadcrumbs;
use common\models\Users;

/* @var $this yii\web\View */
/* @var $searchModel common\models\DocumentsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Documents');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="documents-index">

    <div class="row bg-title">
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title"><?= Html::encode($this->title) ?></h4> 
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12"></div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <?=  Breadcrumbs::widget([
                  'homeLink' => [ 'label' => Yii::t('app', 'Dashboard'), 'url' => Yii::$app->homeUrl,],
                  'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
               ]) ?>
        </div>
    </div>

    

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <div class="white-box">

    <p>
        <?php if (Yii::$app->user->identity->role == Users::ROLE_ADMIN || Yii::$app->user->identity->role == Users::ROLE_SUPERUSER):
                echo Html::a(Yii::t('app', 'Load Document'), ['create'], ['class' => 'btn btn-success btn-rounded']);

                $botones = [
                    'class' => 'kartik\grid\ActionColumn', 
                    'template' => '{link}&nbsp;{edit}&nbsp;{delete}',
                    'buttons' => [
                        'link' => function($url, $model, $key) { 
                            $baseUrl = (new Request)->getBaseUrl();    // render your custom button
                            return Html::a('<i class="fa fa-cloud-download"></i>', $baseUrl.'/'.$model->document,
                                ['class' => 'btn-lg text-info', 'title' => Yii::t('app', 'Download'), 'target' =>'_blank']);
                        },
                        'edit' => function($url, $model, $key) {
                            return Html::a('<i class="fa fa-pencil-square-o"></i>', ['doc/update', 'id' => $model->id_document], [  'class' => 'btn-lg', 'title' => Yii::t('app', 'Edit'), 'style' => 'color:#81c700;']);
                        },

                        'delete' => function($url, $model, $key) {
                            return Html::a('<i class="fa  fa-trash-o"></i>', ['doc/delete', 'id' => $model->id_document], ['class' => 'btn-lg', 'title' => Yii::t('app', 'Delete'), 'id' => 'btn-del', 'style' => 'color:red;',
                                'data' => [
                                        'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                        'method' => 'post',
                                    ],
                                ]);
                        },
                    ],
                    'contentOptions' =>['style'=>'text-align:center;', 'width' => '30%'],
                    'headerOptions' =>['style'=>'text-align:center;', 'width' => '30%'],
                ];
            else:
                $botones = [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{link}',
                    'header' => Yii::t('app', 'Download'),
                    'buttons' => [
                        'link' => function($url, $model, $key) { 
                            $baseUrl = (new Request)->getBaseUrl();    // render your custom button
                            return Html::a(Yii::t('app', 'Download'), $baseUrl.'/'.$model->document,
                                ['class' => 'btn btn-danger btn-block btn-download btn-rounded', 'target' =>'_blank']);
                        }
                    ],
                    'contentOptions' =>['style'=>'text-align:center;', 'width' => '15%'],
                    'headerOptions' =>['style'=>'text-align:center;', 'width' => '15%'],
                ];
            endif;
        ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            // ['class' => 'yii\grid\SerialColumn'],

            // 'id_document',
            
            [
                'attribute' => 'username',
                'value' => 'user.username',  
                'label' => Yii::t('app', 'Username'), 
                'contentOptions' =>['style'=>'text-align:center;', 'width' => '10%'],
                'headerOptions' =>['style'=>'text-align:center;', 'width' => '10%'], 
            ],
            [
                'attribute' => 'fullname',
                'value' => 'user.fullname', 
                'label' => Yii::t('app', 'Full Name'), 
                'contentOptions' =>['style'=>'text-align:left;', 'width' => '20%'],
                'headerOptions' =>['style'=>'text-align:left;', 'width' => '20%'],  
            ],
           
            [
                'attribute' => 'document_name',
                'label' => Yii::t('app', 'Document Name'),
                'contentOptions' =>['style'=>'text-align:left;', 'width' => '30%'],
                'headerOptions' =>['style'=>'text-align:left;', 'width' => '30%'],
            ],
            [
                'attribute' => 'publish_date',
                'label' => Yii::t('app', 'Publish Date'),
                'contentOptions' =>['style'=>'text-align:center;', 'width' => '15%'],
                'headerOptions' =>['style'=>'text-align:center;', 'width' => '15%'],
            ],
            $botones,
        ],
    ]); ?>

</div>


</div>
