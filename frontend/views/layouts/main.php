<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;
use frontend\assets\DashboardAsset;
use \yii\web\Request;
use common\models\Users;
use yii\db\Expression;

DashboardAsset::register($this);

$baseUrl =  (new Request)->getBaseUrl();

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <script type="text/javascript" src="<?= $baseUrl ?>/vis/dist/vis.js"></script>
    <link href="<?= $baseUrl ?>/vis/dist/vis-network.min.css" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" type="image/png" href="<?= $baseUrl ?>/images/favicon.png" />
    <script type="text/javascript" src="<?= $baseUrl ?>/plugins/bower_components/jquery/dist/jquery.min.js"></script>

</head>
<body>
<?php $this->beginBody();

    if (Yii::$app->user->isGuest) { 
        
        echo "<script language='javascript'>window.location='".$baseUrl."/index.php/site/login?v=5j6kynkdjfnseofdj50jteirjsldfj0w9mi4ervfcjkwm03954vreñokrfw4q4m5t65763m4cwe3xkc5945bm6mc90cm395mcweoreiqit91234iwerucm459mtj868jg686jgv68m4042ewr4m3kiweor9345mvifr495mv9ir49m4i5uwermv9343werwve&msg=4'</script>;";

    }
    else{ ?>

<div class="preloader">
        <div class="cssload-speeding-wheel"></div>
    </div>
    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top m-b-0">
            <div class="navbar-header"> <a class="navbar-toggle hidden-sm hidden-md hidden-lg " href="javascript:void(0)" data-toggle="collapse" data-target=".navbar-collapse"><i class="ti-menu"></i></a>
                <div class="top-left-part">
                    <a class="logo" href="index.html"><b> 
                    <?php
                            echo Html::img('@web/images/logo.png',[
                                    'class' => 'img',
                                    'alt' => 'Logo CIMHE',
                                    'width'=> '180',
                            ]) 
                        ?></b>
                    </a>
                </div>
                <ul class="nav navbar-top-links navbar-left hidden-xs">
                    <li><a href="javascript:void(0)" class="open-close hidden-xs waves-effect waves-light"><i class="icon-arrow-left-circle ti-menu"></i></a></li>
                    
                </ul>
                <ul class="nav navbar-top-links navbar-right pull-right">
                   
                    <li>
                        <a href="http://www.cimhe.co" class="" title="Inicio"><b> Inicio</b></a>
                    </li>
                    <li>
                        <?php if (Yii::$app->user->identity->role == Users::ROLE_USER): ?>
                            <a href="<?= $baseUrl ?>/doc/index" class="" title="Entrega Resultados"><b> Entrega Resultados</b></a>
                        <?php else: ?>
                            <a href="<?= $baseUrl ?>/doc/index" class="" title="<?= Yii::t('app', 'Documents') ?>"><b> 
                                <?= Yii::t('app', 'Documents') ?></b></a>
                        <?php endif; ?>
                    </li>
                    <?php if (Yii::$app->user->identity->role == Users::ROLE_SUPERUSER || Yii::$app->user->identity->role == Users::ROLE_ADMIN): ?>
                    <li>
                        <a href="<?= $baseUrl ?>/users/index" class="" title="<?= Yii::t('app', 'Users') ?>"><b> <?= Yii::t('app', 'Users') ?></b></a>
                    </li>
                        
                    <?php endif; ?>
                    <li>
                        <a href="<?= $baseUrl ?>/site/logout" class="" title="<?= Yii::t('app', 'Logout') ?>"><b> 
                            <?= Yii::t('app', 'Logout') ?></b></a>
                    </li>
                    
                   
                   
                </ul>
            </div>
            <!-- /.navbar-header -->
            <!-- /.navbar-top-links -->
            <!-- /.navbar-static-side -->
        </nav>
        
        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                
                 <?= Alert::widget() ?>
                <?= $content ?>
               
               
                
               
                
            </div>
            <!-- /.container-fluid -->
            
        </div>
        <!-- /#page-wrapper -->
    </div>

<?php } ?>  

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; <?= Html::encode(Yii::$app->name) ?> <?= date('Y') ?></p>

        <p class="pull-right">Desarrolado por <a href="http:\\emporiocreativo.com" target="_blank">Emporio Creativo</a></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
