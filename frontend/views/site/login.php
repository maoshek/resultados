<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use \yii\web\Request;

$this->title = Yii::t('app','Login');

$baseUrl =  (new Request)->getBaseUrl();

if (isset($_GET["msg"])) {
    $msg = $_GET["msg"];
}



if ($msg == 1) {
    $message = Yii::t('app', 'Congratulations registration carried out correctly');
    $alert = "success";
    $display = "block";
}else if ($msg == 2) {
    $message = Yii::t('app', 'There was an error when registering');
    $alert = "danger";
    $display = "block";
} 
else if ($msg == 3) {
    $message = Yii::t('app', 'Wrong user or password');
    $alert = "warning";
    $display = "block";
}
else if ($msg == 4) {
    $message = Yii::t('app', 'Login again');
    $alert = "warning";
    $display = "block";
} 
else if ($msg == 5) {
    $message = Yii::t('app', 'You have not activated the account yet, check your email');
    $alert = "warning";
    $display = "block";
} 

else if ($msg == 500) {
    $message = Yii::t('app', 'Datos Incorrectos: '.$errormsj);
    $alert = "danger";
    $display = "block";
} 
else {
    $message = null;
    $alert = "info";
    $display = "none";
}

?>

<section id="wrapper" class="login-register" >

    <div class="login-box">
        <div style ="display:<?= $display ?>" class="alert alert-<?= $alert ?>"> <?= $message ?></div>
        <div class="white-box" style="box-shadow: 0 0 100px rgba(0,0,0,.1)">
            <div class="login-logo">
         <?php echo Html::img('@web/images/logo.png', ['class' =>'img-responsive', 'style' => 'margin-bottom:20px']) ?>
    </div>
            
            <!-- <div style ="display:block" class="alert alert-warning"> Probando</div> -->
            <div class="site-login">
                <!-- <h1><?= Html::encode($this->title) ?></h1> -->

                <?php $form = ActiveForm::begin([
                    'method' => 'post',
                    'id' => 'login-form',
                    'enableClientValidation' => false,
                    'enableAjaxValidation' => true,
                    'options' => ['class' => 'form-horizontal'],
                    'fieldConfig' => [
                        'template' => "<div class=\"col-lg-12\">{input}</div><div class=\"col-lg-12\">{error}</div>",
                    ],       
                    ]); ?>

                    <?= $form->field($model, 'username', [
                        'inputOptions' => ['placeholder' => Yii::t('app',"Code"), 'autofocus' => 'autofocus', 'class' => 'form-control']
                        ])->textInput(['autofocus' => true])
                    ?>

                    <?= $form->field($model, 'password', [
                        'inputOptions' => ['placeholder' => Yii::t('app',"Password"), 'autofocus' => 'autofocus', 'class' => 'form-control']
                        ])->input("password") ?>

                     <!-- <div style="color:#999;margin:1em 0">
                        <?= Yii::t('app','If you forgot your password, you can') ?> <?= Html::a(Yii::t('app','reset it'), ['site/recoverpass']) ?>.
                    </div> -->

                    <div class="form-group">
                        <?= Html::submitButton(Yii::t('app','Login'), ['class' => 'btn btn-primary btn-block btn-login', 'name' => 'login-button']) ?>
                    </div>

                   <!--  <div class="form-group m-b-0">
                            <div class="col-sm-12 text-center">
                                <p><?= Yii::t('app',"¿Don't have an account?") ?> <?= Html::a(Yii::t('app','Sign Up'), ['register']) ?></p>
                            </div>
                        </div> -->

                <?php ActiveForm::end(); ?>

            </div>
        </div>
    </div>
</section>
