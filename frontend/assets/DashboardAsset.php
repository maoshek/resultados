<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class DashboardAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'bootstrap/dist/css/bootstrap.min.css',
        'plugins/bower_components/bootstrap-extension/css/bootstrap-extension.css',
        'plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css',
        'plugins/bower_components/datatables/jquery.dataTables.min.css',
        'plugins/bower_components/sweetalert/sweetalert.css',
        'plugins/bower_components/toast-master/css/jquery.toast.css',
        'plugins/bower_components/morrisjs/morris.css',
        'plugins/bower_components/dropify/dist/css/dropify.min.css',
        'css/animate.css',
        'css/style.css',
        'css/site.css',
        'css/colors/blue.css',

        // 'vis/dist/vis-network.min.css',
    ];


    public $js = [   
        // 'plugins/bower_components/jquery/dist/jquery.min.js',
        'bootstrap/dist/js/tether.min.js',
        'bootstrap/dist/js/bootstrap.min.js',
        'plugins/bower_components/bootstrap-extension/js/bootstrap-extension.min.js',
        'plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js',
        'js/jquery.slimscroll.js',
        'js/waves.js', 
        'js/custom.min.js',
        'plugins/bower_components/sweetalert/sweetalert.min.js',
        'plugins/bower_components/sweetalert/jquery.sweet-alert.custom.js',
        'plugins/bower_components/waypoints/lib/jquery.waypoints.js',
        'plugins/bower_components/counterup/jquery.counterup.min.js',
        'plugins/bower_components/raphael/raphael-min.js',
        'plugins/bower_components/morrisjs/morris.js',
        // 'js/morris-data.js',
        'js/dashboard1.js',
        'plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js',
        'plugins/bower_components/jquery-sparkline/jquery.charts-sparkline.js',
        'plugins/bower_components/toast-master/js/jquery.toast.js',
        'plugins/bower_components/dropify/dist/js/dropify.min.js',
        'plugins/bower_components/knob/jquery.knob.js',
        'js/jasny-bootstrap.js',
        'js/reloj.js',
        'js/dropify.js',
        'plugins/bower_components/peity/jquery.peity.min.js',
        'plugins/bower_components/peity/jquery.peity.init.js',
        'plugins/bower_components/styleswitcher/jQuery.style.switcher.js',
        'js/main.js',
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
