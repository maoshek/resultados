<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class LoginAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'bootstrap/dist/css/bootstrap.min.css',
        'plugins/bower_components/bootstrap-extension/css/bootstrap-extension.css',
        'plugins/bower_components/toast-master/css/jquery.toast.css',
        'css/animate.css',
        'css/style.css',
        'css/site.css',
        'css/colors/default-dark.css',
    ];


    public $js = [   
        'plugins/bower_components/jquery/dist/jquery.min.js',
        'bootstrap/dist/js/tether.min.js',
        'bootstrap/dist/js/bootstrap.min.js',
        'plugins/bower_components/bootstrap-extension/js/bootstrap-extension.min.js',
        'js/jquery.slimscroll.js',
        'js/waves.js', 
        'plugins/bower_components/waypoints/lib/jquery.waypoints.js',
        'plugins/bower_components/morrisjs/morris.js',
        'js/dashboard1.js',
        'plugins/bower_components/toast-master/js/jquery.toast.js',
        'js/jasny-bootstrap.js',
        'js/main.js',
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
