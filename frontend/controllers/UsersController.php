<?php

namespace frontend\controllers;

use Yii;
use common\models\Users;
use common\models\UsersSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use aryelds\sweetalert\SweetAlert;

/**
 * UsersController implements the CRUD actions for Users model.
 */
class UsersController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'create', 'update', 'delete'],
                'rules' => [
                    [
                        'actions' => [''],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['index','create', 'update', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            $valid_roles = [Users::ROLE_ADMIN, Users::ROLE_SUPERUSER];
                            return Users::roleInArray($valid_roles) && Users::isActive();
                        }
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Users models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (Yii::$app->user->isGuest || Yii::$app->user->identity->role == Users::ROLE_USER) {
            
            return $this->redirect(["login"]);

        }
        else{

            if(Yii::$app->user->identity->role == Users::ROLE_SUPERUSER){
                $roles = array(Users::ROLE_USER, Users::ROLE_ADMIN, Users::ROLE_SUPERUSER);
            }
            elseif(Yii::$app->user->identity->role == Users::ROLE_ADMIN){
                $roles = array(Users::ROLE_USER);
            }

            $searchModel = new UsersSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $roles);

            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
    }

    /**
     * Displays a single Users model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Users model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if (Yii::$app->user->isGuest || Yii::$app->user->identity->role == Users::ROLE_USER) {
            
            return $this->redirect(["login"]);

        }
        else{
            $model = new Users();

            if ($model->load(Yii::$app->request->post())) {

                $now = (new \yii\db\Query)->select(new \yii\db\Expression('NOW()'))->scalar();

                $model->password = password_hash($model->password, PASSWORD_BCRYPT);
                $model->auth_key = $model->randKey("abcdef0123456789", 32);
                $model->accessToken = $model->randKey("abcdef0123456789", 200);
                $model->created_at = $now;
                $model->updated_at = $now;
                $model->status_user = Users::STATUS_ACTIVE;

                // echo '<pre>';
                // echo print_r($model);
                // echo die;

                if($model->validate()):
                    $model->save();
                    return $this->redirect(['index']);
                else: 
                    // echo print_r($model->getErrors());
                    // die;
                endif;
            }

            return $this->render('create', [
                'model' => $model,
            ]);    
        }
        
    }

    /**
     * Updates an existing Users model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {

            
            $model->password = password_hash($model->password, PASSWORD_BCRYPT);
            if($model->save()):
                return $this->redirect(['index']);
            endif;
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Users model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Users model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Users the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Users::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
