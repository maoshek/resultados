<?php
namespace frontend\controllers;

use frontend\models\ResendVerificationEmailForm;
use frontend\models\VerifyEmailForm;
use Yii;
use yii\base\InvalidArgumentException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use yii\helpers\Html;
use common\models\Users;
use yii\web\Session;
use yii\web\Cookie;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'login'],
                'rules' => [
                    [
                        'actions' => ['login'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout', 'index','login'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['index','login','logout'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            $valid_roles = [Users::ROLE_ADMIN, Users::ROLE_SUPERUSER];
                            return Users::roleInArray($valid_roles) && Users::isActive();
                        }
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [

                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        if (Yii::$app->user->isGuest) {
            
            return $this->redirect(["login"]);

        } else {

            return $this->redirect(["doc/index"]);
        }
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */

     public function getLanguage(){
        $languages = explode(";", $_SERVER['HTTP_ACCEPT_LANGUAGE']);
        if(strpos($languages[0], "es") !== FALSE){
            $language = "es";
        }
        elseif(strpos($languages[0], "en") !== FALSE){
            $language = "en";
        }
        //Ante cualquier otro idioma devolvemos "es"
        if($language <> "es" && $language <> "en"){
            $language = "es";
        }
        return $language;
    }

    public function actionLogin() {

        if (Yii::$app->user->isGuest) {
            $this->layout = 'loginLayout';
            $msg = null;
             Yii::$app->language = $this->getLanguage();
             
            if (isset($_GET["msg"])) {
                $msg = $_GET["msg"];
            }
            $model = new LoginForm();
            $user=null;

            $username = Html::encode(Yii::$app->request->post('LoginForm')['username']);

            if ($username) {

                $user = Users::findByUsername($username);
           
                if ($model->load(Yii::$app->request->post()) && $model->login()) {
                    // echo '<script> console.log("Si entró model-login '.$user->id.'") </script>';
                     // echo die;
                    $session = new Session;
                    $session->open();
                    // $session['username'] = Yii::$app->user->identity->username;
                    $session['userId'] = Yii::$app->user->identity->id;
                    $session['email'] = Yii::$app->user->identity->email;
                    $session['role'] = Yii::$app->user->identity->role;

                    // echo die;
                    return $this->goBack();
                   
                } 
                
                else {

                    $model->username = Yii::$app->request->post('LoginForm')['username'];
                    $model->password = Yii::$app->request->post('LoginForm')['password'];

                    if(empty($user)){
                              $msg=3;
                       return $this->render('login', ['model' => $model, 'msg' => $msg]);
                
                    }
                    else if(!$user->validatePassword(Yii::$app->request->post('LoginForm')['password'])){
                            $msg=3;
                            return $this->render('login', ['model' => $model, 'msg' => $msg]);
                    }
                    else if ($model->load(Yii::$app->request->post()) && $model->login()) {

                        $session = new Session;
                        $session->open();
                        // $session['username'] = Yii::$app->user->identity->username;
                        $session['username'] = Yii::$app->user->identity->username;
                        $session['email'] = Yii::$app->user->identity->email;
                        $session['role'] = Yii::$app->user->identity->role;

                        return $this->goBack();
                    }
                }
                
            } 
            else {
                return $this->render('login', ['model' => $model, 'msg' => $msg]);
            }
        }
        else {
            return $this->redirect(['doc/index']);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending your message.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post()) && $model->signup()) {
            Yii::$app->session->setFlash('success', 'Thank you for registration. Please check your inbox for verification email.');
            return $this->goHome();
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidArgumentException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    /**
     * Verify email address
     *
     * @param string $token
     * @throws BadRequestHttpException
     * @return yii\web\Response
     */
    public function actionVerifyEmail($token)
    {
        try {
            $model = new VerifyEmailForm($token);
        } catch (InvalidArgumentException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }
        if ($user = $model->verifyEmail()) {
            if (Yii::$app->user->login($user)) {
                Yii::$app->session->setFlash('success', 'Your email has been confirmed!');
                return $this->goHome();
            }
        }

        Yii::$app->session->setFlash('error', 'Sorry, we are unable to verify your account with provided token.');
        return $this->goHome();
    }

    /**
     * Resend verification email
     *
     * @return mixed
     */
    public function actionResendVerificationEmail()
    {
        $model = new ResendVerificationEmailForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');
                return $this->goHome();
            }
            Yii::$app->session->setFlash('error', 'Sorry, we are unable to resend verification email for the provided email address.');
        }

        return $this->render('resendVerificationEmail', [
            'model' => $model
        ]);
    }
}
