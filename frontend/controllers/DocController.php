<?php

namespace frontend\controllers;

use Yii;
use common\models\Documents;
use common\models\DocumentsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\Users;
use \yii\web\Request;
use yii\web\UploadedFile;
use aryelds\sweetalert\SweetAlert;

/**
 * DocController implements the CRUD actions for Documents model.
 */
class DocController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'create', 'update', 'delete'],
                'rules' => [
                    [
                        'actions' => [''],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['index','create', 'update', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            $valid_roles = [Users::ROLE_ADMIN, Users::ROLE_SUPERUSER];
                            return Users::roleInArray($valid_roles) && Users::isActive();
                        }
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Documents models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (Yii::$app->user->isGuest) {
            
            return $this->redirect(["login"]);

        }
        else{

            if (Yii::$app->user->identity->role == Users::ROLE_USER){

                $user_id = Yii::$app->user->identity->id;

            }
            else{

                $user_id = null;            
            }

            $searchModel = new DocumentsSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $user_id);

            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
    }

    /**
     * Displays a single Documents model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Documents model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if (Yii::$app->user->isGuest) {
            
            return $this->redirect(["login"]);

        }
        
        else if (Yii::$app->user->identity->role == Users::ROLE_ADMIN || Yii::$app->user->identity->role == Users::ROLE_SUPERUSER){    

            $model = new Documents();
            $baseUrl =  (new Request)->getBaseUrl();
            $now = (new \yii\db\Query)->select(new \yii\db\Expression('NOW()'))->scalar();

            if ($model->load(Yii::$app->request->post())) {

                $model->publish_date = $now;


                $imagename = null;
                $val = 0;

                $user = Users::findOne($model->user_id);

                if (UploadedFile::getInstance($model, 'file_doc')) {

                    $imagename = $model->document_name.' - '.$user->fullname;
                    $model->file_doc = UploadedFile::getInstance($model, 'file_doc');
                    $model->document = 'docs/' . $imagename . '.' . $model->file_doc->extension;
                    $val = 1;
                }

                if ($val == 1) {
                    $model->save();  

                    if ($imagename) {
                        $model->file_doc->saveAs('docs/' . $imagename . '.' . $model->file_doc->extension);
                    }

                    SweetAlert::widget([
                        'options' => [
                            'title' => "Cargado Exitosamente",
                            // 'text' => "Gracias por verificar su cuenta, le informaremos cuando el proceso haya concluido.",
                            'type' => SweetAlert::TYPE_SUCCESS,
                            'animation' => 'slide-from-top',
                            'theme' => SweetAlert::THEME_GOOGLE,
                            'showCancelButton' => false,
                            'confirmButtonColor' => "#DD6B55",
                            'confirmButtonText' => "Gracias",
                            'closeOnConfirm' => false,
                            'closeOnCancel' => false
                        ],
                        'callbackJs' => new \yii\web\JsExpression(' function(isConfirm) {
                            if (isConfirm) { 
                                window.location="'.$baseUrl.'"
                            } 
                        }')
                    ]);

                    return $this->redirect(['doc/index']);

                    
                }
                else{
                    SweetAlert::widget([
                        'options' => [
                            'title' => "Debes subir el documento",
                            // 'text' => "Hay datos errados",
                            'type' => SweetAlert::TYPE_ERROR,
                            'animation' => 'slide-from-top',
                            'theme' => SweetAlert::THEME_GOOGLE
                        ]
                        ]);
                }

                return $this->redirect(['view', 'id' => $model->id_document]);
            }

            return $this->render('create', [
                'model' => $model,
            ]);
        }
             
        else{
            return $this->redirect(["login"]);

        }
        
    }

    /**
     * Updates an existing Documents model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_document]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Documents model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Documents model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Documents the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Documents::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
